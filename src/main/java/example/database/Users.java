package example.database;

import example.model.User;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
@Cacheable
public class Users {

    private List<User> USERS = Arrays.asList(
            new User("siims", "Siim", "Salin", 53333111),
            new User("karlk", "Karl", "Kama", 530202022)
    );

    private Users() {}

    public List<User> getUsers() {
        return Collections.unmodifiableList(USERS);
    }
}
