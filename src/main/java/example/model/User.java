package example.model;

public class User {

    private String username;
    private String firstName;
    private String lastName;
    private Integer mobileNumber;

    public User(String username, String firstName, String lastName, Integer mobileNumber) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mobileNumber = mobileNumber;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Integer getMobileNumber() {
        return mobileNumber;
    }
}
