package example.controller;

import example.model.SearchForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
class IndexController implements Controllers {

    @GetMapping("/")
    public String load(Model model) {
        model.addAttribute("searchForm", new SearchForm());
        return getTemplate();
    }

    @Override
    public String getTemplate() {
        return "index";
    }
}
