package example.controller;

import example.model.SearchForm;
import example.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.Resource;

@Controller
class UsersListController implements Controllers {

    @Resource
    private UserService userService;

    @PostMapping("/users-list")
    public String getUsersList(@ModelAttribute SearchForm searchForm, Model model) {
        model.addAttribute("user", userService.findUserByUsername(searchForm.getUsername()));
        return getTemplate();
    }

    @GetMapping("/users-list")
    public String redirect(Model model) {
        return "redirect:/";
    }

    @Override
    public String getTemplate() {
        return "usersList";
    }
}
