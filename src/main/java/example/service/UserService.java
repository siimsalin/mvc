package example.service;

import example.database.Users;
import example.model.User;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.annotation.Resource;

@Service
public class UserService {

    @Resource
    private Users users;

    public User findUserByUsername(String username) {
        return users.getUsers().stream()
                .filter(item -> StringUtils.equalsIgnoreCase(item.getUsername(), username))
                .findFirst()
                .orElse(null);
    }
}
