package example.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SearchFormTest {

    private SearchForm testSearchForm;

    @Before
    public void setUp() {
        this.testSearchForm = new SearchForm();
    }

    @Test
    public void setUsername() {
        this.testSearchForm.setUsername("UserName");
        assertEquals("UserName", this.testSearchForm.getUsername());
    }
}