package example.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {

    private User testUser;

    @Before
    public void setUp () {
        this.testUser = new User("TestName", "TestFirstName", "TestLastName", 987654321);
    }

    @Test
    public void getUsername() {
        assertEquals("TestName", this.testUser.getUsername());
    }

    @Test
    public void getFirstName() {
        assertEquals("TestFirstName", this.testUser.getFirstName());
    }

    @Test
    public void getLastName() {
        assertEquals("TestLastName", this.testUser.getLastName());
    }

    @Test
    public void getMobileNumber() {
        assertEquals(Integer.toString(987654321), Integer.toString(this.testUser.getMobileNumber()));
    }
}