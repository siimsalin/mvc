package example.controller;

import example.model.SearchForm;
import example.model.User;
import example.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(MockitoJUnitRunner.class)
public class UserListControllerTest {

    @Spy
    @InjectMocks
    private UsersListController usersListController;

    @Mock
    private UserService userService;

    private MockMvc mockMvc;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(usersListController).build();
    }

    @Test
    public void redirectTest() throws Exception {
        ResultActions resultActions = mockMvc.perform(get("/users-list"));
        resultActions.andExpect(status().is3xxRedirection());
        resultActions.andExpect(view().name("redirect:/"));
    }

    @Test
    public void getUserListTest() throws Exception {
        SearchForm searchForm = new SearchForm();
        searchForm.setUsername("siims");
        User toReturn = new User("siims", "Siim", "Salin", 5999999);
        doReturn(toReturn).when(userService).findUserByUsername(searchForm.getUsername());
        ResultActions resultActions = mockMvc.perform(post("/users-list").param("username", searchForm.getUsername()));
        resultActions.andExpect(status().isOk());
        resultActions.andExpect(view().name("usersList"));
        resultActions.andExpect(model().attribute("user", toReturn));
    }
}
