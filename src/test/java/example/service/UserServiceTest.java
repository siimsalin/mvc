package example.service;

import example.database.Users;
import example.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Spy
    @InjectMocks
    protected UserService userService;

    @Mock
    protected Users usersDb;


    @Test
    public void findUserByUsername() {
        List<User> usersList = new ArrayList<>();
        User user = new User("testUser", "Test", "User", 5333333);
        usersList.add(user);
        Mockito.doReturn(usersList).when(usersDb).getUsers();
        User returnedUser = userService.findUserByUsername("testUser");
        assertEquals(user.getUsername(), returnedUser.getUsername());
        assertEquals(user.getFirstName(), returnedUser.getFirstName());
        assertEquals(user.getLastName(), returnedUser.getLastName());
        assertEquals(user.getMobileNumber(), returnedUser.getMobileNumber());
    }
}